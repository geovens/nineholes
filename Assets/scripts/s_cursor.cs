﻿using UnityEngine;
using System.Collections;

public class s_cursor : MonoBehaviour
{
	s_Data Data;

	// Use this for initialization
	void Start ()
	{
		Data = GameObject.Find("Data").GetComponent<s_Data>();
		if (Data == null) Debug.Log("ERROR: Data == null");
	}
	
	// Update is called once per frame
	void Update ()
	{
		float targetpositionx = Data.CursorI;
		float targetpositiony = Data.CursorJ;
		targetpositionx = 2 * targetpositionx - 3 - 0.114f;
		targetpositiony = 2 * targetpositiony - 2 - 0.114f;

		float positionx;
		float positiony;
		positionx = (targetpositionx + this.transform.position.x) / 2;
		positiony = (targetpositiony + this.transform.position.y) / 2;

		this.transform.position = new Vector3(positionx, positiony);
	}
}
