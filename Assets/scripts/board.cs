﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Board
{
	// 0=placing stones, 1=moving stones, 2=finished
	public int Phase = 0;
	// 0=black, 1=white
	public int Turn = 0;
	// number of moves already played currently
	public int MoveNumber = 0;
	// list of past moves in order
	public List<string> MoveHistory = new List<string>();

	// whether stones are placed
	public bool[,] Placed = new bool[2, 3];
	// the horizontal position of stones
	public int[,] I = new int[2, 3];
	// the vertical position of stones
	public int[,] J = new int[2, 3];

	System.Random Random = new System.Random();

	public Board Clone()
	{
		Board clone = new Board();
		clone.Phase = Phase;
		clone.Turn = Turn;
		clone.MoveNumber = MoveNumber;
		clone.MoveHistory = new List<string>(MoveHistory);
		for (int c = 0; c < 2; c++)
			for (int s = 0; s < 3; s++)
			{
				clone.Placed[c, s] = Placed[c, s];
				clone.I[c, s] = I[c, s];
				clone.J[c, s] = J[c, s];
			}

		return clone;
	}

	public void Clear()
	{
		Phase = 0;
		Turn = 0;
		MoveNumber = 0;
		MoveHistory.Clear();
		for (int c = 0; c < 2; c++)
			for (int s = 0; s < 3; s++)
			{
				Placed[c, s] = false;
				I[c, s] = -1;
				J[c, s] = -1;
			}
	}

	// when placing stones, move="p00" where 00 is the coordinate
	// when moving stones, move="u0" where u could be u,d,l,r and 0 indicates stone index
	public int Play(string move)
	{
		if (move == "")
		{
			Debug.Log("WARNING: Play(\"\")");
			return -1;
		}

		if (Phase == 0)
		{
			int c = Turn;
			int s = MoveNumber / 2;
			int targeti = int.Parse(move[1].ToString());
			int targetj = int.Parse(move[2].ToString());

			if (targeti < 0 || targetj < 0 || targeti > 2 || targetj > 2)
				return -1;
			if (Color(targeti, targetj) >= 0)
				return -1;

			Placed[c, s] = true;
			I[c, s] = targeti;
			J[c, s] = targetj;
		}
		else if (Phase == 1)
		{
			int c = Turn;
			int s = -1;
			int targeti = -1;
			int targetj = -1;
			if (move.Contains("0"))
				s = 0;
			if (move.Contains("1"))
				s = 1;
			if (move.Contains("2"))
				s = 2;

			int i = I[c, s];
			int j = J[c, s];

			if (move.Contains("u"))
			{
				targeti = i;
				targetj = j + 1;
			}
			else if (move.Contains("d"))
			{
				targeti = i;
				targetj = j - 1;
			}
			else if (move.Contains("l"))
			{
				targeti = i - 1;
				targetj = j;
			}
			else if (move.Contains("r"))
			{
				targeti = i + 1;
				targetj = j;
			}

			if (targeti < 0 || targetj < 0 || targeti > 2 || targetj > 2)
				return -1;
			if (Color(targeti, targetj) >= 0)
				return -1;

			I[c, s] = targeti;
			J[c, s] = targetj;
		}
		
		MoveNumber++;
		Turn = 1 - Turn;
		if (MoveNumber == 6)
			Phase = 1;
		if (Win() >= 0)
			Phase = 2;

		MoveHistory.Add(move);
		//Debug.Log(move);

		return 0;
	}

	public int Undo()
	{
		if (MoveNumber == 0)
			return -1;

		List<string> mh = new List<string>(MoveHistory);
		mh.RemoveAt(mh.Count - 1);
		Clear();
		foreach (string move in mh)
			Play(move);
		return 0;
	}

	public List<string> ListPossibleMoves()
	{
		List<string> moves = new List<string>();

		if (Phase == 0)
		{
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					if (Color(i, j) == -1)
						moves.Add("p" + i.ToString() + j.ToString());
		}
		else if (Phase == 1)
		{
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					if (Color(i, j) == -1)
					{
						// up
						int origini = i;
						int originj = j - 1;
						if (originj >= 0 && Color(origini, originj) == Turn)
							moves.Add("u" + StoneIndex(origini, originj).ToString());

						// down
						origini = i;
						originj = j + 1;
						if (originj < 3 && Color(origini, originj) == Turn)
							moves.Add("d" + StoneIndex(origini, originj).ToString());

						// left
						origini = i + 1;
						originj = j;
						if (origini < 3 && Color(origini, originj) == Turn)
							moves.Add("l" + StoneIndex(origini, originj).ToString());

						// right
						origini = i - 1;
						originj = j;
						if (origini >= 0 && Color(origini, originj) == Turn)
							moves.Add("r" + StoneIndex(origini, originj).ToString());
					}
		}

		return moves;
	}

	public string GenerateRandomMove()
	{
		List<string> moves = ListPossibleMoves();
		if (moves.Count == 0)
			return "";

		int select = Random.Next(0, moves.Count);
		string move = moves[select];
		return move;
	}

	public string GenerateAIMove()
	{
		List<string> moves = ListPossibleMoves();

		// play if win
		foreach (string winmove in moves)
		{
			Board t = Clone();
			t.Play(winmove);
			if (t.Win() >= 0)
			{
				//Debug.Log("a win move");
				return winmove;
			}
		}

		// play if opponent cannot win in next move
		List<string> okmoves = new List<string>();
		foreach (string searchmove in moves)
		{
			Board t1 = Clone();
			t1.Play(searchmove);
			List<string> opmoves = t1.ListPossibleMoves();
			bool opwin = false;
			foreach (string opmove in opmoves)
			{
				Board t2 = t1.Clone();
				t2.Play(opmove);
				if (t2.Win() >= 0)
				{
					opwin = true;
					break;
				}
			}
			if (!opwin)
			{
				okmoves.Add(searchmove);
			}
		}
		if (okmoves.Count > 0)
		{
			//Debug.Log(okmoves.Count.ToString() + " ok moves");
			string theokmove;
			int limittick = 0; // make AI occasionaly not play the urgent move
			do
			{
				limittick++;
				theokmove = GenerateRandomMove();
			}
			while (!okmoves.Contains(theokmove) && limittick < 6);
			return theokmove;
		}


		// random if losing whatever
		//Debug.Log("oh my god i'm losing");
		string move = GenerateRandomMove();
		return move;
	}

	// return color of a specific position. -1=no stone, 0=black, 1=white
	public int Color(int i, int j)
	{
		if (i < 0 || j < 0 || i > 2 || j > 2)
			Debug.Log("ERROR: Color() out of range");

		for (int c = 0; c < 2; c++)
			for (int s = 0; s < 3; s++)
				if (Placed[c, s] && I[c, s] == i && J[c, s] == j)
					return c;
		return -1;
	}

	// return stone index of a specific position. 
	public int StoneIndex(int i, int j)
	{
		if (i < 0 || j < 0 || i > 2 || j > 2)
			Debug.Log("ERROR: StoneIndex() out of range");
		if (Color(i, j) < 0)
		{
			Debug.Log("WARNING: StoneIndex() no stone");
			return -1;
		}

		for (int c = 0; c < 2; c++)
			for (int s = 0; s < 3; s++)
				if (Placed[c, s] && I[c, s] == i && J[c, s] == j)
					return s;
		return -1;
	}

	// return the result of the current board. -1=not finished, 0=black win, 1=white win
	public int Win()
	{
		for (int winc = 0; winc < 2; winc++)
		{
			for (int i = 0; i < 3; i++)
				if (Color(i, 0) == winc && Color(i, 1) == winc && Color(i, 2) == winc)
					return winc;
			for (int j = 0; j < 3; j++)
				if (Color(0, j) == winc && Color(1, j) == winc && Color(2, j) == winc)
					return winc;
			if (Color(0, 0) == winc && Color(1, 1) == winc && Color(2, 2) == winc)
				return winc;
			if (Color(2, 0) == winc && Color(1, 1) == winc && Color(0, 2) == winc)
				return winc;
		}
		return -1;
	}
}
