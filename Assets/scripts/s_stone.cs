﻿using UnityEngine;
using System.Collections;

public class s_stone : MonoBehaviour
{
	s_Data Data;
	int color;
	int stoneindex;

	public Sprite sprite;
	public Sprite sprite_grabbed;
	public Sprite sprite_win;

	// Use this for initialization
	void Start ()
	{
		Data = GameObject.Find("Data").GetComponent<s_Data>();
		if (Data == null) Debug.Log("ERROR: Data == null");

		if (this.name.Contains("o"))
			color = 1;
		if (this.name.Contains("x"))
			color = 0;
		if (this.name.Contains("0"))
			stoneindex = 0;
		if (this.name.Contains("1"))
			stoneindex = 1;
		if (this.name.Contains("2"))
			stoneindex = 2;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Data.Board.Win() == color)
		{
			GetComponent<SpriteRenderer>().sprite = sprite_win;
		}
		else if (Data.Grabed && Data.GrabedC == color && Data.GrabedS == stoneindex)
		{
			GetComponent<SpriteRenderer>().sprite = sprite_grabbed;
		}
		else
		{
			GetComponent<SpriteRenderer>().sprite = sprite;
		}

		float targetpositionx = Data.Board.I[color, stoneindex];
		float targetpositiony = Data.Board.J[color, stoneindex];
		targetpositionx = 2 * targetpositionx - 3 - 0.114f;
		targetpositiony = 2 * targetpositiony - 2 - 0.114f;

		float positionx;
		float positiony;
		if (Data.Board.MoveNumber > 6)
		{
			positionx = (targetpositionx + this.transform.position.x) / 2;
			positiony = (targetpositiony + this.transform.position.y) / 2;
		}
		else
		{
			positionx = targetpositionx;
			positiony = targetpositiony;
		}

		this.transform.position = new Vector3(positionx, positiony);

		if (Data.Board.Placed[color, stoneindex])
			GetComponent<SpriteRenderer>().enabled = true;
		else
			GetComponent<SpriteRenderer>().enabled = false;
	}
}
