﻿using UnityEngine;
using System.Collections;

public class s_Keyboard : MonoBehaviour
{
	s_Data Data;

	// Use this for initialization
	void Start ()
	{
		Data = GameObject.Find("Data").GetComponent<s_Data>();
		if (Data == null) Debug.Log("ERROR: Data == null");

		Input.imeCompositionMode = IMECompositionMode.Off;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.UpArrow))
			Data.Command = 'u';
		if (Input.GetKeyDown(KeyCode.DownArrow))
			Data.Command = 'd';
		if (Input.GetKeyDown(KeyCode.LeftArrow))
			Data.Command = 'l';
		if (Input.GetKeyDown(KeyCode.RightArrow))
			Data.Command = 'r';
		if (Input.GetKeyDown(KeyCode.Space))
			Data.Command = 'p';
		if (Input.GetKeyDown(KeyCode.G))
			Data.Command = 'g';
		if (Input.GetKeyDown(KeyCode.M))
			Data.Command = 'f';
		if (Input.GetKeyDown(KeyCode.Escape))
			Data.Command = 'e';
		if (Input.GetKeyDown(KeyCode.Z))
			Data.Command = 'z';
		if (Input.GetKeyDown(KeyCode.N))
			Data.Command = 'n';
	}
}
