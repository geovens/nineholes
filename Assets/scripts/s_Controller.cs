﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class s_Controller : MonoBehaviour
{
	const int OpponentWaitTickN = 20;

	s_Data Data;

	int UpdateCount = 0;
	int OpponentWaitTick = 0;

	// Use this for initialization
	void Start ()
	{
		Data = GameObject.Find("Data").GetComponent<s_Data>();
		if (Data == null) Debug.Log("ERROR: Data == null");

		Data.Board.Clear();
	}
	
	// Update is called once per frame
	void Update ()
	{
		int ci = Data.CursorI;
		int cj = Data.CursorJ;
		Board DB = Data.Board;

		// AI plays black
		OpponentWaitTick--;
		if (DB.Phase < 2 && DB.Turn == 0 && OpponentWaitTick < 0)
		{
			string move = DB.GenerateAIMove();
			DB.Play(move);
		}
		

		if (Data.Command != '\0')
		{
			//Debug.Log(Data.Command.ToString());
			switch (Data.Command)
			{
				case 'u':
					Data.CursorJ++;
					if (Data.CursorJ > 2) Data.CursorJ = 2;
					break;
				case 'd':
					Data.CursorJ--;
					if (Data.CursorJ < 0) Data.CursorJ = 0;
					break;
				case 'l':
					Data.CursorI--;
					if (Data.CursorI < 0) Data.CursorI = 0;
					break;
				case 'r':
					Data.CursorI++;
					if (Data.CursorI > 2) Data.CursorI = 2;
					break;
				case 'p':
					if (DB.Phase == 0 && DB.Color(ci, cj) == -1)
					{
						DB.Play("p" + ci.ToString() + cj.ToString());
						OpponentWaitTick = OpponentWaitTickN;
					}
					break;
				case 'g':
					if (!Data.Grabed && DB.Phase == 1 && DB.Color(ci, cj) == DB.Turn)
					{
						Data.GrabedC = DB.Color(ci, cj);
						Data.GrabedS = DB.StoneIndex(ci, cj);
						Data.Grabed = true;
					}
					break;
				case 'f':
					if (Data.Grabed && DB.Phase == 1 && DB.Color(ci, cj) == -1)
					{
						int origini = DB.I[Data.GrabedC, Data.GrabedS];
						int originj = DB.J[Data.GrabedC, Data.GrabedS];
						if (Mathf.Abs(origini - ci) == 1 && originj == cj || Mathf.Abs(originj - cj) == 1 && origini == ci)
						{
							string move = "";
							if (cj == originj + 1)
								move = "u";
							else if (cj == originj - 1)
								move = "d";
							else if (ci == origini - 1)
								move = "l";
							else if (ci == origini + 1)
								move = "r";
							move += Data.GrabedS.ToString();
							DB.Play(move);
							Data.Grabed = false;
							OpponentWaitTick = OpponentWaitTickN;
						}
					}
					break;
				case 'e':
					if (Data.Grabed)
					{
						Data.Grabed = false;
					}
					else if (DB.Win() != 1 && DB.MoveNumber >= 3)
					{
						DB.Undo();
						DB.Undo();
					}
					else if (DB.Win() == 1)
					{
						DB.Clear();
					}
					break;
				case 'z':
					if (DB.Win() != 1 && DB.MoveNumber >= 3)
					{
						DB.Undo();
						DB.Undo();
					}
					break;
				case 'n':
					if (DB.Phase == 2 && DB.Win() == 1)
						DB.Clear();
					break;
			}

			Data.Command = '\0';
		}

		// AI self play
		/*
		UpdateCount++;
		if (UpdateCount % 30 == 0 && UpdateCount > 0)
		{
			if (Data.Board.Phase == 2)
			{
				Data.Board.Clear();
				Debug.ClearDeveloperConsole();
			}
			else
			{
				// random play
				//string move = Data.Board.GenerateRandomMove();
				//Data.Board.Play(move);

				// smarter play
				string move = Data.Board.GenerateAIMove();
				Data.Board.Play(move);
			}
		}
		*/
	}




}
